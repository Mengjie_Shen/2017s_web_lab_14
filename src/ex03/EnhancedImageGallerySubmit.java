package ex03;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EnhancedImageGallerySubmit extends HttpServlet {
//    private int maxFileSize = 5 * 1024 * 1024;
//    private String rootPath;
//    private String thumbnailFile;
    private String session_id;

    private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 50 * 1024 * 1024;
    private int maxMemSize = 4 * 1024 * 1024;
    private File file;

//    public void init( ){
//        // Get the file location where it would be stored.
//        filePath = getServletContext().getInitParameter("file-upload");
//
//    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {
        ServletContext servletContext = getServletContext();
        filePath = servletContext.getRealPath("/Photos");
        //System.out.println("initial filePath " + filePath + "\\");

        Map<String, String[]> picName = request.getParameterMap();

        for (String key : picName.keySet()) {
            System.out.println("in for loop");
            String[] value = picName.get(key);
            System.out.println("<p>" + key + "</p><p>" + Arrays.toString(value) + "</p>");
        }

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter();

        if (!isMultipart) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p>No file uploaded</p>");
            out.println("</body>");
            out.println("</html>");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator i = fileItems.iterator();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");

            while (i.hasNext()) {
                FileItem fi = (FileItem) i.next();
                //System.out.println("Fileitem" + fi);
                if (!fi.isFormField()) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String fileName = fi.getName();
                    System.out.println("fileName " + fileName);
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();


                    session_id = request.getSession().getId();
                    System.out.println("line 116: " + session_id);
                    File theDir = new File(filePath + "\\"+ session_id);
                    System.out.println("line 118: " + theDir.getAbsolutePath());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        System.out.println("line 119 " + theDir.exists());
                    }



                    // Write the file
                    filePath = filePath + "\\" + session_id;
                    System.out.println("line 126: " + filePath);

                    if (fileName.lastIndexOf("\\") >= 0) {
                        file = new File(filePath + "\\" + fileName.substring(fileName.lastIndexOf("\\")));
                    } else {
                        file = new File(filePath + "\\" + fileName.substring(fileName.lastIndexOf("\\") + 1));
                    }
                    System.out.println(file.getAbsolutePath());
//                    System.out.println("line 132: " + filePath + fileName.substring(fileName.lastIndexOf("\\")));
                    fi.write(file);

/*                    File textFile = new File(filePath + "\\" + fi.getString() + ".txt");
                    //System.out.println("fi " + fi.getString());
                    System.out.println("textFile: " + textFile.getPath());
                    try(BufferedWriter writer = new BufferedWriter(new FileWriter(textFile))){
                        writer.write(fi.getString());
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }*/

                    //String[] pic = picName.get(1);

                    //out.println("Uploaded Filename: " + pic[0] + "<br>");
                    System.out.println("HTML tag: <img src='../Photos/" + session_id + "/" + fileName + "'>");
                    out.println("<img src='../Photos/" + session_id + "/" + fileName + "'>");
                } else {
                    //System.out.println(fi);
                    out.println("<p>File name: " + fi.getString() + "</p>");
                    File textFile = new File(filePath + "\\" + fi.getString() + ".txt");
                    //System.out.println("fi " + fi.getString());
                    System.out.println("textFile: " + textFile.getPath());
                    try(BufferedWriter writer = new BufferedWriter(new FileWriter(textFile))){
                        writer.write(fi.getString());
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        throw new ServletException("GET method used with " +
                getClass().getName() + ": POST method required.");
    }

//    public EnhancedImageGallerySubmit() {
//        super();
////        filePath = getServletContext().getInitParameter("file-upload");
//        rootPath = "H:\\Documents\\COMPSCI_719\\2017s_web_lab_14\\web\\Photos\\";
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        try (PrintWriter out = response.getWriter()) {
//            out.println("Please use POST mode!");
//        }
//    }
//
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//        DiskFileItemFactory factory = new DiskFileItemFactory();
//
//        factory.setRepository(new File("H:\\Documents\\COMPSCI_719\\2017s_web_lab_14\\web\\Photos\\"));
//
//        ServletFileUpload upload = new ServletFileUpload(factory);
//
//        upload.setSizeMax(maxFileSize);
//
//        PrintWriter out = response.getWriter();
//
//        try {
//            // Parse the request to get file items.
//            List fileItems = upload.parseRequest(request);
//
//            // Process the uploaded file items
//            Iterator i = fileItems.iterator();
//
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet upload</title>");
//            out.println("</head>");
//            out.println("<body>");
//            File file;
//            while (i.hasNext()) {
//                FileItem fi = (FileItem) i.next();
//                if (!fi.isFormField()) {
//                    // Get the uploaded file parameters
//                    String fieldName = fi.getFieldName();
//                    String fileName = fi.getName();
//                    String contentType = fi.getContentType();
//                    boolean isInMemory = fi.isInMemory();
//                    long sizeInBytes = fi.getSize();
//
//                    // Write the file
//                    String finalName;
//
//                    session_id = request.getSession().getId();
//                    File theDir = new File(session_id);
//                    if (!theDir.exists()) {
//                        theDir.mkdirs();
//                        System.out.println("line 81 " + theDir.exists());
//                    }
//
//                    String filePath = rootPath + session_id + "\\";
//                    Path path = Paths.get(filePath);
//                    Files.createDirectories(path);
//
//                    if (fileName.lastIndexOf("/") >= 0) {
//                        finalName = filePath + fileName.substring(fileName.lastIndexOf("/"));
//                    } else {
//                        finalName = filePath + fileName.substring(fileName.lastIndexOf("/") + 1);
//                    }
//
//                    System.out.println("line 92 " + finalName);
//                    file = new File(finalName);
//                    fi.write(file);
//                    out.println("Uploaded Filename: " + finalName + "<br/>");
//
//                    // thumbnails
//                    BufferedImage img = null;
//                    try {
//                        img = ImageIO.read(new File(finalName));
//                        thumbnailFile = finalName.replace(finalName.substring(finalName.lastIndexOf(".")), "_thumbnail.png");
//                        System.out.println("line 107 " + thumbnailFile);
//                        if (img.getHeight() < 400 && img.getWidth() < 400) {
//
//                            ImageIO.write(img, "png", new File(thumbnailFile));
//                        } else {
//                            double zoom = Math.max(1.0 * img.getHeight() / 400, 1.0 * img.getWidth() / 400);
//                            int type = img.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : img.getType();
//                            BufferedImage resizedImage = new BufferedImage((int) (img.getWidth() / zoom), (int) (img.getHeight() / zoom), type);
//                            Graphics2D g = resizedImage.createGraphics();
//                            g.drawImage(img, 0, 0, (int) (img.getWidth() / zoom), (int) (img.getHeight() / zoom), null);
//                            g.dispose();
//                            g.setComposite(AlphaComposite.Src);
//                            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//                            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//                            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//                            ImageIO.write(resizedImage, "png", new File(thumbnailFile));
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    String fieldName = fi.getFieldName();
//                    if (fieldName.equals("desc")) {
//                        String desc = fi.getString();
//                        out.println("The description is: " + desc + "<br/>");
//                    }
//                }
//            }
//            out.println("</body>");
//            out.println("</html>");
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
//
//        File file = new File(thumbnailFile);
//
//        File fi = new File(file.getPath().replace("_thumbnail.png", ".jpg"));
//
//        out.println("<div style=\"width:200px;\"><fieldset>");
//        out.println("<legend>" + file.getName().substring(0, file.getName().lastIndexOf("_")) + "</legend>");
//        out.println("<a href=\"../Photos/" + session_id + "/" + fi.getName() + "\">");
//        out.println("<img src=\"../Photos/" + session_id + "/" + file.getName() + "\" /></a>");
//
//        out.println("<br/>Size:" + fi.length());
//        out.println("</fieldset></div>");


}

